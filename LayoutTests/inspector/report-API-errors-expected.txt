Tests that InspectorBackendStub is catching incorrect arguments.

Protocol Error: Invalid type of argument 'enabled' for method 'ConsoleAgent.setMonitoringXHREnabled' call. It should be 'boolean' but it is 'number'.
Protocol Error: Invalid number of arguments for method 'ConsoleAgent.setMonitoringXHREnabled' call. It should have the next arguments '{"enabled":{"optional":false,"type":"boolean"}}'.
Protocol Error: Optional callback argument for method 'ConsoleAgent.setMonitoringXHREnabled' call should be a function but its type is 'string'.
Protocol Error: the message is for non-existing domain 'wrongDomain'
Protocol Error: Attempted to dispatch an unimplemented method 'Inspector.something-strange'

