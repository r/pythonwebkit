layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x540
  RenderBlock {HTML} at (0,0) size 800x540
    RenderBody {BODY} at (8,16) size 784x494
      RenderBlock {P} at (0,0) size 784x34
        RenderText {#text} at (0,2) size 573x29
          text run at (0,2) width 573: "Tests that origin of rotation is the center of the border box, with and without box-sizing"
      RenderBlock {DIV} at (30,64) size 200x200 [bgcolor=#FF0000]
      RenderBlock {DIV} at (30,294) size 200x200 [bgcolor=#FF0000]
layer at (38,80) size 200x200
  RenderBlock {DIV} at (0,0) size 200x200 [bgcolor=#008000] [border: (100px solid #808080) none]
    RenderText {#text} at (0,2) size 72x63
      text run at (0,2) width 72: "box-sizing:"
      text run at (0,36) width 28: "auto"
layer at (38,310) size 200x200
  RenderBlock {DIV} at (0,0) size 200x200 [bgcolor=#008000] [border: (100px solid #808080) none]
    RenderText {#text} at (0,2) size 72x63
      text run at (0,2) width 72: "box-sizing:"
      text run at (0,36) width 28: "auto"
