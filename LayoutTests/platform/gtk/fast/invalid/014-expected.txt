layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock (anonymous) at (0,0) size 784x19
        RenderText {#text} at (0,0) size 599x19
          text run at (0,0) width 282: "Random tests of some bizarre combinations. "
          text run at (282,0) width 317: "H2 should allow a form inside it, but p should not."
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {FORM} at (0,19) size 784x31
        RenderMenuList {SELECT} at (2,2) size 39x27 [bgcolor=#FFFFFF] [border: (1px solid #000000)]
          RenderBlock (anonymous) at (1,1) size 37x25
            RenderText at (3,3) size 11x19
              text run at (3,3) width 11: "A"
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {P} at (0,66) size 784x0
layer at (470,46) size 47x59
  RenderBlock (positioned) {H2} at (470,46) size 47x59 [border: (2px solid #008000)]
    RenderBlock {FORM} at (2,2) size 43x31
      RenderMenuList {SELECT} at (2,2) size 39x27 [bgcolor=#FFFFFF] [border: (1px solid #000000)]
        RenderBlock (anonymous) at (1,1) size 37x25
          RenderText at (3,3) size 11x19
            text run at (3,3) width 11: "A"
      RenderText {#text} at (0,0) size 0x0
layer at (470,43) size 4x4
  RenderBlock (positioned) {P} at (470,43) size 4x4 [border: (2px solid #008000)]
