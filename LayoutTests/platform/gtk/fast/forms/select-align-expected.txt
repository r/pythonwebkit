layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x19
        RenderText {#text} at (0,0) size 575x19
          text run at (0,0) width 575: "The following select elements should all be rendered on the left, with their text left justified."
      RenderBlock (anonymous) at (0,35) size 784x155
        RenderMenuList {SELECT} at (0,2) size 300x27 [bgcolor=#FFFFFF] [border: (1px solid #000000)]
          RenderBlock (anonymous) at (1,1) size 298x25
            RenderText at (3,3) size 209x19
              text run at (3,3) width 209: "This is should be left justified."
        RenderText {#text} at (300,6) size 4x19
          text run at (300,6) width 4: " "
        RenderBR {BR} at (0,0) size 0x0
        RenderMenuList {SELECT} at (0,33) size 300x27 [bgcolor=#FFFFFF] [border: (1px solid #000000)]
          RenderBlock (anonymous) at (1,1) size 298x25
            RenderText at (3,3) size 209x19
              text run at (3,3) width 209: "This is should be left justified."
        RenderText {#text} at (300,37) size 4x19
          text run at (300,37) width 4: " "
        RenderBR {BR} at (0,0) size 0x0
        RenderMenuList {SELECT} at (0,64) size 300x27 [bgcolor=#FFFFFF] [border: (1px solid #000000)]
          RenderBlock (anonymous) at (1,1) size 298x25
            RenderText at (3,3) size 209x19
              text run at (3,3) width 209: "This is should be left justified."
        RenderText {#text} at (300,68) size 4x19
          text run at (300,68) width 4: " "
        RenderBR {BR} at (0,0) size 0x0
        RenderMenuList {SELECT} at (0,95) size 300x27 [bgcolor=#FFFFFF] [border: (1px solid #000000)]
          RenderBlock (anonymous) at (1,1) size 298x25
            RenderText at (3,3) size 209x19
              text run at (3,3) width 209: "This is should be left justified."
        RenderText {#text} at (300,99) size 4x19
          text run at (300,99) width 4: " "
        RenderBR {BR} at (0,0) size 0x0
        RenderMenuList {SELECT} at (0,126) size 300x27 [bgcolor=#FFFFFF] [border: (1px solid #000000)]
          RenderBlock (anonymous) at (1,1) size 298x25
            RenderText at (3,3) size 209x19
              text run at (3,3) width 209: "This is should be left justified."
        RenderText {#text} at (0,0) size 0x0
        RenderText {#text} at (0,0) size 0x0
      RenderBlock {DIV} at (0,190) size 784x31
        RenderMenuList {SELECT} at (0,2) size 300x27 [bgcolor=#FFFFFF] [border: (1px solid #000000)]
          RenderBlock (anonymous) at (1,1) size 298x25
            RenderText at (3,3) size 209x19
              text run at (3,3) width 209: "This is should be left justified."
