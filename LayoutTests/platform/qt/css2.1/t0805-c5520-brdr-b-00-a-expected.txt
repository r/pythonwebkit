layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x103
  RenderBlock {HTML} at (0,0) size 800x103
    RenderBody {BODY} at (8,16) size 784x71
      RenderBlock {P} at (0,0) size 784x32 [color=#0000FF] [border: (10px double #0000FF) none]
        RenderText {#text} at (0,0) size 405x22
          text run at (0,0) width 405: "This paragraph should have two blue lines directly under it."
      RenderBlock {P} at (0,48) size 784x23 [color=#0000FF] [border: (1px solid #0000FF) none]
        RenderText {#text} at (0,0) size 455x22
          text run at (0,0) width 455: "This paragraph should have a single thin blue line directly under it."
