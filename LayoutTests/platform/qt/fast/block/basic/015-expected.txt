layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x484
  RenderBlock {HTML} at (0,0) size 800x484
    RenderBody {BODY} at (8,21) size 784x447
      RenderBlock {H1} at (0,0) size 784x42
        RenderText {#text} at (0,0) size 457x42
          text run at (0,0) width 457: "Minimum and Maximum Widths"
      RenderBlock {DIV} at (0,63) size 784x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 17x17
          RenderText {#text} at (37,6) size 17x17
            text run at (37,6) width 17: "div"
        RenderText {#text} at (54,3) size 440x22
          text run at (54,3) width 440: " should have a medium solid purple border, as should all the rest."
      RenderBlock {DIV} at (0,91) size 319x72 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 17x17
          RenderText {#text} at (37,6) size 17x17
            text run at (37,6) width 17: "div"
        RenderText {#text} at (54,3) size 308x44
          text run at (54,3) width 201: " should have a width of 40%. "
          text run at (255,3) width 56: "This is a"
          text run at (3,25) width 67: "reference "
        RenderInline {CODE} at (0,0) size 17x17
          RenderText {#text} at (70,28) size 17x17
            text run at (70,28) width 17: "div"
        RenderText {#text} at (87,25) size 192x22
          text run at (87,25) width 192: " and should work as long as "
        RenderInline {CODE} at (0,0) size 30x17
          RenderText {#text} at (279,28) size 30x17
            text run at (279,28) width 30: "width"
        RenderText {#text} at (3,47) size 46x22
          text run at (3,47) width 46: "works."
      RenderBlock {DIV} at (0,163) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 17x17
          RenderText {#text} at (37,6) size 17x17
            text run at (37,6) width 17: "div"
        RenderText {#text} at (54,3) size 197x22
          text run at (54,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,191) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 17x17
          RenderText {#text} at (37,6) size 17x17
            text run at (37,6) width 17: "div"
        RenderText {#text} at (54,3) size 197x22
          text run at (54,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,219) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 17x17
          RenderText {#text} at (37,6) size 17x17
            text run at (37,6) width 17: "div"
        RenderText {#text} at (54,3) size 197x22
          text run at (54,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,247) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 17x17
          RenderText {#text} at (37,6) size 17x17
            text run at (37,6) width 17: "div"
        RenderText {#text} at (54,3) size 197x22
          text run at (54,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,275) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 17x17
          RenderText {#text} at (37,6) size 17x17
            text run at (37,6) width 17: "div"
        RenderText {#text} at (54,3) size 197x22
          text run at (54,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,303) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 17x17
          RenderText {#text} at (37,6) size 17x17
            text run at (37,6) width 17: "div"
        RenderText {#text} at (54,3) size 197x22
          text run at (54,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,331) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 17x17
          RenderText {#text} at (37,6) size 17x17
            text run at (37,6) width 17: "div"
        RenderText {#text} at (54,3) size 197x22
          text run at (54,3) width 197: " should have a width of 40%."
      RenderBlock {DIV} at (0,359) size 319x28 [border: (3px solid #800080)]
        RenderText {#text} at (3,3) size 34x22
          text run at (3,3) width 34: "This "
        RenderInline {CODE} at (0,0) size 17x17
          RenderText {#text} at (37,6) size 17x17
            text run at (37,6) width 17: "div"
        RenderText {#text} at (54,3) size 197x22
          text run at (54,3) width 197: " should have a width of 40%."
      RenderBlock {P} at (0,403) size 784x44
        RenderText {#text} at (0,0) size 217x22
          text run at (0,0) width 217: "If the browser does not support "
        RenderInline {CODE} at (0,0) size 55x17
          RenderText {#text} at (217,3) size 55x17
            text run at (217,3) width 55: "min-width"
        RenderText {#text} at (272,0) size 34x22
          text run at (272,0) width 34: " and "
        RenderInline {CODE} at (0,0) size 59x17
          RenderText {#text} at (306,3) size 59x17
            text run at (306,3) width 59: "max-width"
        RenderText {#text} at (365,0) size 185x22
          text run at (365,0) width 185: ", then the widths should be "
        RenderInline {CODE} at (0,0) size 25x17
          RenderText {#text} at (550,3) size 25x17
            text run at (550,3) width 25: "auto"
        RenderText {#text} at (575,0) size 8x22
          text run at (575,0) width 8: ", "
        RenderInline {CODE} at (0,0) size 26x17
          RenderText {#text} at (583,3) size 26x17
            text run at (583,3) width 26: "40%"
        RenderText {#text} at (609,0) size 8x22
          text run at (609,0) width 8: ", "
        RenderInline {CODE} at (0,0) size 26x17
          RenderText {#text} at (617,3) size 26x17
            text run at (617,3) width 26: "30%"
        RenderText {#text} at (643,0) size 8x22
          text run at (643,0) width 8: ", "
        RenderInline {CODE} at (0,0) size 26x17
          RenderText {#text} at (651,3) size 26x17
            text run at (651,3) width 26: "50%"
        RenderText {#text} at (677,0) size 8x22
          text run at (677,0) width 8: ", "
        RenderInline {CODE} at (0,0) size 26x17
          RenderText {#text} at (685,3) size 26x17
            text run at (685,3) width 26: "50%"
        RenderText {#text} at (711,0) size 8x22
          text run at (711,0) width 8: ", "
        RenderInline {CODE} at (0,0) size 26x17
          RenderText {#text} at (719,3) size 26x17
            text run at (719,3) width 26: "40%"
        RenderText {#text} at (745,0) size 8x22
          text run at (745,0) width 8: ", "
        RenderInline {CODE} at (0,0) size 26x17
          RenderText {#text} at (753,3) size 26x17
            text run at (753,3) width 26: "30%"
        RenderText {#text} at (779,0) size 4x22
          text run at (779,0) width 4: ","
        RenderInline {CODE} at (0,0) size 26x17
          RenderText {#text} at (0,25) size 26x17
            text run at (0,25) width 26: "40%"
        RenderText {#text} at (26,22) size 8x22
          text run at (26,22) width 8: ", "
        RenderInline {CODE} at (0,0) size 25x17
          RenderText {#text} at (34,25) size 25x17
            text run at (34,25) width 25: "auto"
        RenderText {#text} at (59,22) size 209x22
          text run at (59,22) width 209: " (with 70% margin-right), and "
        RenderInline {CODE} at (0,0) size 25x17
          RenderText {#text} at (268,25) size 25x17
            text run at (268,25) width 25: "auto"
        RenderText {#text} at (293,22) size 4x22
          text run at (293,22) width 4: "."
