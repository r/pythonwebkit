layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x576
      RenderBlock {P} at (0,0) size 784x44 [color=#008000]
        RenderText {#text} at (0,0) size 753x44
          text run at (0,0) width 753: "This text should be green if the resolution scaling factor is 1.0, purple if the scaling factor is 1.5, red if the scaling"
          text run at (0,22) width 226: "factor is 2.0, and black otherwise."
