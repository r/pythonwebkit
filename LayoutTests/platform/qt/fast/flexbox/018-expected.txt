layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x44
        RenderText {#text} at (0,0) size 774x44
          text run at (0,0) width 551: "You should see a 350x350 pixel (7 by 7) black/white checkerboard pattern below. "
          text run at (551,0) width 223: "This test is checking to see if box-"
          text run at (0,22) width 175: "pack: justify is supported."
      RenderFlexibleBox {DIV} at (0,60) size 358x358 [border: (4px solid #800000)]
        RenderFlexibleBox {DIV} at (4,4) size 50x350
          RenderFlexibleBox {DIV} at (0,0) size 50x50 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,100) size 50x50 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,200) size 50x50 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,300) size 50x50 [bgcolor=#000000]
        RenderFlexibleBox {DIV} at (54,4) size 50x350 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,0) size 50x50 [bgcolor=#FFFFFF]
          RenderFlexibleBox {DIV} at (0,100) size 50x50 [bgcolor=#FFFFFF]
          RenderFlexibleBox {DIV} at (0,200) size 50x50 [bgcolor=#FFFFFF]
          RenderFlexibleBox {DIV} at (0,300) size 50x50 [bgcolor=#FFFFFF]
        RenderFlexibleBox {DIV} at (104,4) size 50x350
          RenderFlexibleBox {DIV} at (0,0) size 50x50 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,100) size 50x50 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,200) size 50x50 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,300) size 50x50 [bgcolor=#000000]
        RenderFlexibleBox {DIV} at (154,4) size 50x350 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,0) size 50x50 [bgcolor=#FFFFFF]
          RenderFlexibleBox {DIV} at (0,100) size 50x50 [bgcolor=#FFFFFF]
          RenderFlexibleBox {DIV} at (0,200) size 50x50 [bgcolor=#FFFFFF]
          RenderFlexibleBox {DIV} at (0,300) size 50x50 [bgcolor=#FFFFFF]
        RenderFlexibleBox {DIV} at (204,4) size 50x350
          RenderFlexibleBox {DIV} at (0,0) size 50x50 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,100) size 50x50 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,200) size 50x50 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,300) size 50x50 [bgcolor=#000000]
        RenderFlexibleBox {DIV} at (254,4) size 50x350 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,0) size 50x50 [bgcolor=#FFFFFF]
          RenderFlexibleBox {DIV} at (0,100) size 50x50 [bgcolor=#FFFFFF]
          RenderFlexibleBox {DIV} at (0,200) size 50x50 [bgcolor=#FFFFFF]
          RenderFlexibleBox {DIV} at (0,300) size 50x50 [bgcolor=#FFFFFF]
        RenderFlexibleBox {DIV} at (304,4) size 50x350
          RenderFlexibleBox {DIV} at (0,0) size 50x50 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,100) size 50x50 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,200) size 50x50 [bgcolor=#000000]
          RenderFlexibleBox {DIV} at (0,300) size 50x50 [bgcolor=#000000]
