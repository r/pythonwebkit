layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x44
        RenderText {#text} at (0,0) size 57x22
          text run at (0,0) width 57: "Test for "
        RenderInline {I} at (0,0) size 778x44
          RenderInline {A} at (0,0) size 304x22 [color=#0000EE]
            RenderText {#text} at (57,0) size 304x22
              text run at (57,0) width 304: "http://bugs.webkit.org/show_bug.cgi?id=13194"
          RenderText {#text} at (361,0) size 778x44
            text run at (361,0) width 4: " "
            text run at (365,0) width 413: "REGRESSION: Selection rects are wrong for images in search"
            text run at (0,22) width 204: "results from images.google.com"
        RenderText {#text} at (204,22) size 4x22
          text run at (204,22) width 4: "."
      RenderBlock {P} at (0,60) size 784x22
        RenderText {#text} at (0,0) size 767x22
          text run at (0,0) width 767: "Tests highlighting of replaced objects in table cells with extra height. You should see a dark green box and no red."
      RenderTable {TABLE} at (0,98) size 56x104
        RenderTableSection {TBODY} at (0,0) size 56x104
          RenderTableRow {TR} at (0,2) size 56x100
            RenderTableCell {TD} at (2,26) size 52x52 [r=0 c=0 rs=1 cs=1]
              RenderImage {IMG} at (1,1) size 50x50 [bgcolor=#FF0000]
selection start: position 0 of child 0 {IMG} of child 1 {TD} of child 0 {TR} of child 1 {TBODY} of child 5 {TABLE} of body
selection end:   position 1 of child 0 {IMG} of child 1 {TD} of child 0 {TR} of child 1 {TBODY} of child 5 {TABLE} of body
