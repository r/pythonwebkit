layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x44
        RenderText {#text} at (0,0) size 111x22
          text run at (0,0) width 111: "This is a test for "
        RenderInline {I} at (0,0) size 777x44
          RenderInline {A} at (0,0) size 352x22 [color=#0000EE]
            RenderText {#text} at (111,0) size 352x22
              text run at (111,0) width 352: "http://bugzilla.opendarwin.org/show_bug.cgi?id=9316"
          RenderText {#text} at (463,0) size 777x44
            text run at (463,0) width 4: " "
            text run at (467,0) width 310: "REGRESSION: text field width shrinks on first"
            text run at (0,22) width 61: "keystroke"
        RenderText {#text} at (61,22) size 4x22
          text run at (61,22) width 4: "."
      RenderBlock {HR} at (0,60) size 784x2 [border: (1px inset #000000)]
layer at (8,78) size 784x0
  RenderBlock (relative positioned) {DIV} at (0,70) size 784x0
    RenderTextControl {INPUT} at (0,2) size 392x26
layer at (10,82) size 388x22
  RenderBlock {DIV} at (2,2) size 388x22
    RenderText {#text} at (1,0) size 21x22
      text run at (1,0) width 21: "foo"
