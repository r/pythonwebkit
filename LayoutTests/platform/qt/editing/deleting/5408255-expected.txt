ALERT: 22
ALERT: 100
layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x44
        RenderText {#text} at (0,0) size 742x44
          text run at (0,0) width 742: "This tests for a bug where the delete button wouldn't work because it had -webkit-user-select:none instead of -"
          text run at (0,22) width 177: "webkit-user-select:ignore. "
          text run at (177,22) width 478: "The list should be removed, the editable region below should be empty."
      RenderBlock {DIV} at (0,60) size 784x54
caret: position 0 of child 3 {DIV} of body
