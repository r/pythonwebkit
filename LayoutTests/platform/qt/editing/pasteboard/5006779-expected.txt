layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x600
  RenderBlock {HTML} at (0,0) size 800x600
    RenderBody {BODY} at (8,8) size 784x584
      RenderBlock {P} at (0,0) size 784x22
        RenderText {#text} at (0,0) size 647x22
          text run at (0,0) width 454: "This tests copying/pasting less than a paragraph of quoted content. "
          text run at (454,0) width 193: "It should not appear quoted."
      RenderBlock {DIV} at (0,38) size 784x110
        RenderBlock (anonymous) at (0,0) size 784x44
          RenderText {#text} at (0,0) size 21x22
            text run at (0,0) width 21: "On"
          RenderBR {BR} at (21,16) size 0x0
          RenderBR {BR} at (0,22) size 0x22
        RenderBlock {BLOCKQUOTE} at (0,44) size 784x22 [color=#0000FF] [border: (2px solid #0000FF)]
          RenderText {#text} at (12,0) size 174x22
            text run at (12,0) width 174: "On Tuesday, Dave wrote:"
        RenderBlock {BLOCKQUOTE} at (0,66) size 784x22 [color=#0000FF] [border: (2px solid #0000FF)]
          RenderBR {BR} at (12,0) size 0x22
        RenderBlock {BLOCKQUOTE} at (0,88) size 784x22 [color=#0000FF] [border: (2px solid #0000FF)]
          RenderText {#text} at (12,0) size 87x22
            text run at (12,0) width 87: "Hello World."
caret: position 2 of child 1 {#text} of child 2 {DIV} of body
