layer at (0,0) size 784x802
  RenderView at (0,0) size 784x600
layer at (0,0) size 784x802
  RenderBlock {HTML} at (0,0) size 784x802
    RenderBody {BODY} at (8,8) size 768x786 [color=#008000] [bgcolor=#CCCCCC]
      RenderBlock {P} at (0,0) size 768x22
        RenderText {#text} at (0,0) size 380x22
          text run at (0,0) width 380: "The style declarations which apply to the text below are:"
      RenderBlock {PRE} at (0,38) size 768x119
        RenderText {#text} at (0,0) size 186x119
          text run at (0,0) width 121: "BODY {color: green;}"
          text run at (121,0) width 0: " "
          text run at (0,17) width 92: "H3 {color: blue;}"
          text run at (92,17) width 0: " "
          text run at (0,34) width 107: "EM {color: purple;}"
          text run at (107,34) width 0: " "
          text run at (0,51) width 130: ".one {font-style: italic;}"
          text run at (130,51) width 0: " "
          text run at (0,68) width 186: ".two {text-decoration: underline;}"
          text run at (186,68) width 0: " "
          text run at (0,85) width 107: "#two {color: navy;}"
          text run at (107,85) width 0: " "
          text run at (0,102) width 120: ".three {color: purple;}"
          text run at (120,102) width 0: " "
      RenderBlock {HR} at (0,170) size 768x2 [border: (1px inset #008000)]
      RenderBlock {H3} at (0,190) size 768x26 [color=#0000FF]
        RenderText {#text} at (0,0) size 220x26
          text run at (0,0) width 220: "This sentence should show "
        RenderInline {STRONG} at (0,0) size 35x26
          RenderText {#text} at (220,0) size 35x26
            text run at (220,0) width 35: "blue"
        RenderText {#text} at (255,0) size 42x26
          text run at (255,0) width 42: " and "
        RenderInline {EM} at (0,0) size 51x26 [color=#800080]
          RenderText {#text} at (297,0) size 51x26
            text run at (297,0) width 51: "purple"
        RenderText {#text} at (348,0) size 5x26
          text run at (348,0) width 5: "."
      RenderBlock {H3} at (0,234) size 768x26 [color=#0000FF]
        RenderText {#text} at (0,0) size 197x26
          text run at (0,0) width 197: "This sentence should be "
        RenderInline {SPAN} at (0,0) size 34x26
          RenderText {#text} at (197,0) size 34x26
            text run at (197,0) width 34: "blue"
        RenderText {#text} at (231,0) size 104x26
          text run at (231,0) width 104: " throughout."
      RenderBlock {P} at (0,278) size 768x22
        RenderText {#text} at (0,0) size 241x22
          text run at (0,0) width 241: "This should be green except for the "
        RenderInline {EM} at (0,0) size 118x22 [color=#800080]
          RenderText {#text} at (241,0) size 118x22
            text run at (241,0) width 118: "emphasized words"
        RenderText {#text} at (359,0) size 171x22
          text run at (359,0) width 171: ", which should be purple."
      RenderBlock {H3} at (0,318) size 768x26 [color=#0000FF]
        RenderText {#text} at (0,0) size 296x26
          text run at (0,0) width 296: "This should be blue and underlined."
      RenderBlock {P} at (0,362) size 768x22
        RenderText {#text} at (0,0) size 313x22
          text run at (0,0) width 313: "This sentence should be underlined, including "
        RenderInline {TT} at (0,0) size 47x17
          RenderText {#text} at (313,3) size 47x17
            text run at (313,3) width 47: "this part"
        RenderText {#text} at (360,0) size 8x22
          text run at (360,0) width 8: ", "
        RenderInline {I} at (0,0) size 53x22
          RenderText {#text} at (368,0) size 53x22
            text run at (368,0) width 53: "this part"
        RenderText {#text} at (421,0) size 8x22
          text run at (421,0) width 8: ", "
        RenderInline {EM} at (0,0) size 53x22 [color=#800080]
          RenderText {#text} at (429,0) size 53x22
            text run at (429,0) width 53: "this part"
        RenderText {#text} at (482,0) size 38x22
          text run at (482,0) width 38: ", and "
        RenderInline {STRONG} at (0,0) size 57x22
          RenderText {#text} at (520,0) size 57x22
            text run at (520,0) width 57: "this part"
        RenderText {#text} at (577,0) size 4x22
          text run at (577,0) width 4: "."
      RenderBlock {P} at (0,400) size 768x22 [color=#000080]
        RenderText {#text} at (0,0) size 464x22
          text run at (0,0) width 464: "This sentence should also be underlined, as well as dark blue (navy), "
        RenderInline {TT} at (0,0) size 102x17
          RenderText {#text} at (464,3) size 102x17
            text run at (464,3) width 102: "including this part"
        RenderText {#text} at (566,0) size 4x22
          text run at (566,0) width 4: "."
      RenderBlock {P} at (0,438) size 768x22 [color=#800080]
        RenderText {#text} at (0,0) size 284x22
          text run at (0,0) width 284: "This sentence should be purple, including "
        RenderInline {STRONG} at (0,0) size 57x22
          RenderText {#text} at (284,0) size 57x22
            text run at (284,0) width 57: "this part"
        RenderText {#text} at (341,0) size 34x22
          text run at (341,0) width 34: " and "
        RenderInline {SPAN} at (0,0) size 187x22
          RenderText {#text} at (375,0) size 187x22
            text run at (375,0) width 187: "this part (which is spanned)"
        RenderText {#text} at (562,0) size 4x22
          text run at (562,0) width 4: "."
      RenderTable {TABLE} at (0,476) size 603x310 [border: (1px outset #808080)]
        RenderTableSection {TBODY} at (1,1) size 601x308
          RenderTableRow {TR} at (0,0) size 601x30
            RenderTableCell {TD} at (0,0) size 601x30 [bgcolor=#C0C0C0] [border: (1px inset #808080)] [r=0 c=0 rs=1 cs=2]
              RenderInline {STRONG} at (0,0) size 163x22
                RenderText {#text} at (4,4) size 163x22
                  text run at (4,4) width 163: "TABLE Testing Section"
          RenderTableRow {TR} at (0,30) size 601x278
            RenderTableCell {TD} at (0,154) size 12x30 [bgcolor=#C0C0C0] [border: (1px inset #808080)] [r=1 c=0 rs=1 cs=1]
              RenderText {#text} at (4,4) size 4x22
                text run at (4,4) width 4: " "
            RenderTableCell {TD} at (12,30) size 589x278 [border: (1px inset #808080)] [r=1 c=1 rs=1 cs=1]
              RenderBlock {H3} at (4,4) size 581x26 [color=#0000FF]
                RenderText {#text} at (0,0) size 220x26
                  text run at (0,0) width 220: "This sentence should show "
                RenderInline {STRONG} at (0,0) size 35x26
                  RenderText {#text} at (220,0) size 35x26
                    text run at (220,0) width 35: "blue"
                RenderText {#text} at (255,0) size 42x26
                  text run at (255,0) width 42: " and "
                RenderInline {EM} at (0,0) size 51x26 [color=#800080]
                  RenderText {#text} at (297,0) size 51x26
                    text run at (297,0) width 51: "purple"
                RenderText {#text} at (348,0) size 5x26
                  text run at (348,0) width 5: "."
              RenderBlock {H3} at (4,48) size 581x26 [color=#0000FF]
                RenderText {#text} at (0,0) size 197x26
                  text run at (0,0) width 197: "This sentence should be "
                RenderInline {SPAN} at (0,0) size 34x26
                  RenderText {#text} at (197,0) size 34x26
                    text run at (197,0) width 34: "blue"
                RenderText {#text} at (231,0) size 104x26
                  text run at (231,0) width 104: " throughout."
              RenderBlock {P} at (4,92) size 581x22
                RenderText {#text} at (0,0) size 241x22
                  text run at (0,0) width 241: "This should be green except for the "
                RenderInline {EM} at (0,0) size 118x22 [color=#800080]
                  RenderText {#text} at (241,0) size 118x22
                    text run at (241,0) width 118: "emphasized words"
                RenderText {#text} at (359,0) size 171x22
                  text run at (359,0) width 171: ", which should be purple."
              RenderBlock {H3} at (4,132) size 581x26 [color=#0000FF]
                RenderText {#text} at (0,0) size 296x26
                  text run at (0,0) width 296: "This should be blue and underlined."
              RenderBlock {P} at (4,176) size 581x22
                RenderText {#text} at (0,0) size 313x22
                  text run at (0,0) width 313: "This sentence should be underlined, including "
                RenderInline {TT} at (0,0) size 47x17
                  RenderText {#text} at (313,3) size 47x17
                    text run at (313,3) width 47: "this part"
                RenderText {#text} at (360,0) size 8x22
                  text run at (360,0) width 8: ", "
                RenderInline {I} at (0,0) size 53x22
                  RenderText {#text} at (368,0) size 53x22
                    text run at (368,0) width 53: "this part"
                RenderText {#text} at (421,0) size 8x22
                  text run at (421,0) width 8: ", "
                RenderInline {EM} at (0,0) size 53x22 [color=#800080]
                  RenderText {#text} at (429,0) size 53x22
                    text run at (429,0) width 53: "this part"
                RenderText {#text} at (482,0) size 38x22
                  text run at (482,0) width 38: ", and "
                RenderInline {STRONG} at (0,0) size 57x22
                  RenderText {#text} at (520,0) size 57x22
                    text run at (520,0) width 57: "this part"
                RenderText {#text} at (577,0) size 4x22
                  text run at (577,0) width 4: "."
              RenderBlock {P} at (4,214) size 581x22 [color=#000080]
                RenderText {#text} at (0,0) size 464x22
                  text run at (0,0) width 464: "This sentence should also be underlined, as well as dark blue (navy), "
                RenderInline {TT} at (0,0) size 102x17
                  RenderText {#text} at (464,3) size 102x17
                    text run at (464,3) width 102: "including this part"
                RenderText {#text} at (566,0) size 4x22
                  text run at (566,0) width 4: "."
              RenderBlock {P} at (4,252) size 581x22 [color=#800080]
                RenderText {#text} at (0,0) size 284x22
                  text run at (0,0) width 284: "This sentence should be purple, including "
                RenderInline {STRONG} at (0,0) size 57x22
                  RenderText {#text} at (284,0) size 57x22
                    text run at (284,0) width 57: "this part"
                RenderText {#text} at (341,0) size 34x22
                  text run at (341,0) width 34: " and "
                RenderInline {SPAN} at (0,0) size 187x22
                  RenderText {#text} at (375,0) size 187x22
                    text run at (375,0) width 187: "this part (which is spanned)"
                RenderText {#text} at (562,0) size 4x22
                  text run at (562,0) width 4: "."
