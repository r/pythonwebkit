layer at (0,0) size 800x600
  RenderView at (0,0) size 800x600
layer at (0,0) size 800x486
  RenderBlock {html} at (0,0) size 800x486
    RenderBody {body} at (8,16) size 784x462
      RenderBlock {p} at (0,0) size 784x44
        RenderText {#text} at (0,0) size 766x44
          text run at (0,0) width 766: "The svg area contained in the div element (red box), should fill out the whole area (blue rectangle), especially after"
          text run at (0,22) width 279: "resizing the content box to a different size"
      RenderBlock {div} at (0,60) size 402x402 [border: (1px solid #FF0000)]
        RenderSVGRoot {svg} at (9,77) size 400x400
          RenderSVGPath {rect} at (9,77) size 400x400 [fill={[type=SOLID] [color=#0000FF]}] [x=0.00] [y=0.00] [width=400.00] [height=400.00]
        RenderText {#text} at (0,0) size 0x0
